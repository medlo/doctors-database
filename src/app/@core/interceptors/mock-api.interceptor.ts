import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse,
} from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { delay } from 'rxjs/operators';

@Injectable()
export class MockApiInterceptor implements HttpInterceptor {
  private doctorsArray = [
    {
      firstName: 'Gunnar',
      lastName: 'Larsson',
      identityNumber: '631203-2345',
      jobType: 'Läkare',
      specialization: 'Allmänläkare',
    },
    {
      firstName: 'Greta',
      lastName: 'Svensson',
      identityNumber: '781103-4511',
      jobType: 'Sjuksköterska',
      specialization: 'Distriktssköterska',
    },
    {
      firstName: 'Lotta',
      lastName: 'Jansson',
      identityNumber: '990106-1199',
      jobType: 'Läkare',
      specialization: 'Specialistläkare neurologi',
    },
    {
      firstName: 'Bertil',
      lastName: 'Olsson',
      identityNumber: '841024-5523',
      jobType: 'Sjuksköterska',
      specialization: 'BVC sjuksköterska',
    },
    {
      firstName: 'Anders',
      lastName: 'Lagerkranz',
      identityNumber: '591121-4521',
      jobType: 'Läkare',
      specialization: 'Kirurg',
    },
  ];

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    if (request?.url === 'http://api.medlo.se/doctors') {
      const artificialDelay = 200 + Math.random() * 1000;
      return of(
        new HttpResponse({ status: 200, body: this.doctorsArray })
      ).pipe(delay(artificialDelay));
    }

    return next.handle(request);
  }
}
