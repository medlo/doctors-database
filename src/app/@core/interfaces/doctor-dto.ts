export interface DoctorDTO {
  firstName: string;
  lastName: string;
}
