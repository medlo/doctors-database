import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DoctorsListComponent } from './components/doctors-list/doctors-list.component';
import { DoctorsRoutingModule } from './doctors-routing.module';
import { DoctorsMainPageComponent } from './pages/doctors-main-page/doctors-main-page.component';
import { DoctorsDialogComponent } from './components/doctors-dialog/doctors-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';

@NgModule({
  declarations: [
    DoctorsListComponent,
    DoctorsMainPageComponent,
    DoctorsDialogComponent,
  ],
  imports: [CommonModule, MatDialogModule, DoctorsRoutingModule],
})
export class DoctorsModule {}
