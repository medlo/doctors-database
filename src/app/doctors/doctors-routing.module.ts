import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DoctorsMainPageComponent } from './pages/doctors-main-page/doctors-main-page.component';

const routes: Routes = [
    { path: '', component: DoctorsMainPageComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: []
})
export class DoctorsRoutingModule {}
