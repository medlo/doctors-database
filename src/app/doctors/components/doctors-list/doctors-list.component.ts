import { Component, Input } from '@angular/core';
import { Observable, of } from 'rxjs';
import { DoctorDTO } from 'src/app/@core/interfaces/doctor-dto';
import { MatDialog } from '@angular/material/dialog';
import { DoctorsDialogComponent } from '../doctors-dialog/doctors-dialog.component';

@Component({
  selector: 'app-doctors-list',
  templateUrl: './doctors-list.component.html',
  styleUrls: ['./doctors-list.component.scss'],
})
export class DoctorsListComponent {
  @Input() public doctors$: Observable<DoctorDTO[]> = of([]);

  constructor(private dialog: MatDialog) {}

  public showDoctor(doctor: DoctorDTO) {
    this.dialog.open(DoctorsDialogComponent, { data: { doctor } });
  }
}
