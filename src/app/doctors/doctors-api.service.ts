import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DoctorDTO } from '../@core/interfaces/doctor-dto';

@Injectable({
  providedIn: 'root',
})
export class DoctorsApiService {
  constructor(private http: HttpClient) {}

  public getDoctors() {
    return this.http.get<DoctorDTO[]>('http://api.medlo.se/doctors');
  }
}
