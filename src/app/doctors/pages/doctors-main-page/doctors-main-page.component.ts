import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { DoctorDTO } from 'src/app/@core/interfaces/doctor-dto';
import { DoctorsApiService } from '../../doctors-api.service';

@Component({
  selector: 'app-doctors-main-page',
  templateUrl: './doctors-main-page.component.html',
  styleUrls: ['./doctors-main-page.component.scss'],
})
export class DoctorsMainPageComponent {
  public doctors$: Observable<DoctorDTO[]>;

  constructor(private doctorsApi: DoctorsApiService) {
    this.doctors$ = this.doctorsApi
      .getDoctors()
      .pipe(map((res) => res.sort(byString('lastName'))));
  }
}

function byString(field: string) {
  return (d1: any, d2: any) => {
    return d1[field].localeCompare(d2[field]);
  };
}
